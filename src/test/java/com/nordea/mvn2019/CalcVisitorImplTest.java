package com.nordea.mvn2019;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.stream.Stream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Java Doc for test class
 */
public class CalcVisitorImplTest {

    CalcVisitorImpl calc = new CalcVisitorImpl();

    @ParameterizedTest
    @MethodSource("argumentsProvider")
    void testExpressions(String expression, double expected) {
        CharStream inputStream = CharStreams.fromString(expression);

        Lexer lexer = new CalcLexer(inputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        CalcParser parser = new CalcParser(tokens);
        ParseTree tree = parser.start();

        double actual = calc.visit(tree);

        assertEquals(expected, actual);
    }

    @Test
    void testZeroDivision() {
        CharStream inputStream = CharStreams.fromString("1/0");

        Lexer lexer = new CalcLexer(inputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        CalcParser parser = new CalcParser(tokens);
        ParseTree tree = parser.start();

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            calc.visit(tree);
        });
    }

    static Stream<Arguments> argumentsProvider() {
        return Stream.of(
                arguments("10",   10.0),
                arguments("1 + 1", 2.0),
                arguments("4 - 5", -1.0),
                arguments("2 * 2", 4.0),
                arguments("8 / 2", 4.0),
                arguments("2 ^ 2", 4.0),
                arguments("((2) + 2)", 4.0),
                arguments("1.25", 1.25),
                arguments("1.25 + 1.25", 2.5),
                arguments("4.5 - 5.5", -1.0),
                arguments("2.5 * 2.5", 6.25),
                arguments("12.5 / 2.5", 5.0),
                arguments("2.5 ^ 2", 6.25),
                arguments("((2.75) + 2.75)", 5.5)
        );
    }
}
