package com.nordea.mvn2019;

import java.util.Scanner;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.tree.ParseTree;

/**
 * Hello world!
 *
 */
public class App {

    public static void main( String[] args ) {
        System.out.println("Nordea 2019 Maven Training - Simple Calculator in Java");
        Scanner in = new Scanner(System.in);

        CalcVisitorImpl calc = new CalcVisitorImpl();

        while (true) {
            System.out.print("Calc > ");
            if (in.hasNextLine()) {
                String input = in.nextLine();
                if (input.toLowerCase().equals("exit")) {
                    break;
                } else {
                    CharStream inputStream = CharStreams.fromString(input);
                    Lexer lexer = new CalcLexer(inputStream);
                    CommonTokenStream tokens = new CommonTokenStream(lexer);
                    CalcParser parser = new CalcParser(tokens);
                    ParseTree tree = parser.start();

                    try {
                        Double result = calc.visit(tree);
                        System.out.println(result);
                    } catch (IllegalArgumentException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            } else {
                System.out.println();
                break;
            }
        }
    }
}
